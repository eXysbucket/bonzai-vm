lock '>=3.2.1'

set :application, 'bonzaidigital-test-recrutement'
# put your repo URL here 
# ex : git@github.com:Peanutz/my_super_repo.git
set :scm, :git
# https://eXysbucket@bitbucket.org/eXysbucket/bonzai.git
#set :repo_url, 'git@bitbucket.org:eXysbucket/bonzai.git'
set :repo_url, 'https://eXysbucket@bitbucket.org/eXysbucket/bonzai.git'
#set :repo_url, 'git@github.com:nihilas/bonzaidigital-demo-tr.git'
# branch, either default or master, that's just you
set :branch, 'master'

set :stages, ["staging", "production"]
set :default_stage, "staging"

set :use_sudo, false
set :deploy_to, "/var/www/#{fetch(:application)}"
set :ssh_options, { :forward_agent => true }

set :format, :pretty
set :log_level, :debug
set :pty, true
# You have node and grunt if you so need it
set :linked_dirs, %w{node_modules}
set :keep_releases, 12
set :grunt_flags, '--no-color'
set :npm_flags, '--silent'

set :mysql_params, "-u root -ptoor"
set :mysql_db_name, "shoppinglist"

task :migrate do
	desc "migrate database on server"
	on "default" do
		execute "touch #{current_path}/migration.list; ls -1v #{current_path}/docs/sql/*.sql 2>/dev/null > #{current_path}/migration.available; diff #{current_path}/migration.available #{current_path}/migration.list | awk \"/^</ {print \\$2}\" | while read f ; do echo \"migrating $(basename $f)\"; mysql #{fetch(:mysql_params)} #{fetch(:mysql_db_name)} < $f && echo $f >> #{current_path}/migration.list ; done; rm -f #{current_path}/migration.available"
	end
end

namespace :setup do
	desc "create database on server"
	task :create_db do
		on "default" do
			execute "mysql #{fetch(:mysql_params)} -e \"CREATE DATABASE IF NOT EXISTS #{fetch(:mysql_db_name)}\""
		end
	end
end

after 'deploy', 'setup:create_db'
after 'setup:create_db', 'migrate'
after 'deploy:updated', 'grunt'
after 'deploy:updated', 'composer:install'

