#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

echo "Pre-install nginx php mysql"
sudo apt-get update

test -x /usr/sbin/nginx || {
	# Force a blank root password for mysql
	sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password toor"
	sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password toor"
	
	sudo apt-get install -y php5-fpm php5-cli nginx mysql-server php5-mysql curl
} &>/dev/null

echo "Configuring Nginx"
test -x /etc/nginx/sites-available/nginx_vhost || {
	sudo cp /vagrant/provision/config/nginx_vhost /etc/nginx/sites-available/nginx_vhost
	ln -s /etc/nginx/sites-available/nginx_vhost /etc/nginx/sites-enabled/nginx_vhost
	rm /etc/nginx/sites-available/default
	rm /etc/nginx/sites-enabled/default
	sudo service nginx restart
	sudo service php5-fpm restart
} &>/dev/null

echo "Pre-install Node, Bower, Grunt"
test -x /usr/local/bin/node || {
  cd /usr/local/src
  wget --quiet http://nodejs.org/dist/v0.10.28/node-v0.10.28-linux-x64.tar.gz
  sudo tar -zxvf node-v0.10.28-linux-x64.tar.gz
  cd /usr/local/bin
  sudo ln -s /usr/local/src/node-v0.10.28-linux-x64/bin/node
  sudo ln -s /usr/local/src/node-v0.10.28-linux-x64/bin/npm
  sudo npm install -g bower grunt-cli grunt uglify-js uglifycss
} &>/dev/null

echo "Pre-install git"
test -x /usr/bin/git || {
	sudo apt-get install -y git
} &>/dev/null

echo "Setup project directory"
test -d /var/www/bonzaidigital-test-recrutement || {
  sudo mkdir -p /var/www/bonzaidigital-test-recrutement
  sudo chown vagrant.vagrant /var/www/bonzaidigital-test-recrutement
} &>/dev/null

echo "Pre-install composer"
test -x /usr/local/bin/composer || {
  curl -sS https://getcomposer.org/installer | php -- --install-dir=/opt
  sudo ln -s /opt/composer.phar /usr/local/bin/composer
}